from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^search/', views.show_search_form, name='show-form'),
    url(r'^save-form/', views.save_form, name='save-form'),
    url(r'^filter-results/(?P<datamodels>.*)/(?P<languages>.*)/(?P<movements>.*)/(?P<page>\d+)', views.filter_results, name='filter-results'),
    
    url(r'^(?P<uri>[-\w%]+)/$', views.nosql_detail, name='nosql_detail'),

    url(r'^api/similar/datamodel', views.get_similar_by_datamodel),
    url(r'^api/similar/stackoverflow', views.get_similar_by_stackoverflow_tags),
    url(r'^api/similar/language', views.get_similar_by_language),
    url(r'^api/similar/performance', views.get_similar_by_performance),

    url(r'^api/repositories-by-language', views.get_repositories_progressbars),
    url(r'^api/sentiments', views.get_sentiment_pie),
    url(r'^api/interest-over-time', views.get_interest_over_time),
    url(r'^api/popularity', views.get_popularity),

    url(r'^api/datamodel/(?P<name>.*)', views.get_nosqls_by_datamodel, name='search_by_datamodel'),

    url(r'^api/languages-repos', views.RepositoriesByLanguageData.as_view()),

]
