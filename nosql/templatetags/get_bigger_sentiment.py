from django import template

register = template.Library()

@register.filter
def get_bigger_sentiment(sentiments):
    positive = sentiments['positive']
    negative = sentiments['negative']
    neutral = sentiments['neutral']
    if positive > negative and positive > neutral:
    	return 'positive'
    elif negative > positive and negative > neutral:
    	return 'negative'
    else:
    	return 'neutral'
