from django import template

register = template.Library()

@register.filter
def get_summary(summaries):
    for summary in summaries:
    	if summary:
    		return summary
    return None