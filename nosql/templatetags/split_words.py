from django import template

register = template.Library()


@register.filter
def split_words(text, num_words):
	text = text.split('.')[0] + '.'
	pieces = text.split()
	sentences = (" ".join(pieces[i:i + num_words]) for i in range(0, len(pieces), num_words))
	return sentences
