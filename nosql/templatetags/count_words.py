from django import template

register = template.Library()

@register.filter
def count_words(text):
	if text:
		words = text.split()
		return len(words) > 1
	return False