from django import template
from fuzzywuzzy import fuzz

register = template.Library()

@register.filter
def clean_operating_systems(ops):
    ops = list(ops)
    ops = [op for op in ops if op not in 'unknown']
    clean_ops = []
    if len(ops) == 1:
        clean_ops = ops
    else:
        for i, value in enumerate(ops):
            pos = i
            while pos > 0 and fuzz.ratio(value, ops[pos - 1]) < 65:
                clean_ops.append(value)
                pos = pos - 1
    return set(clean_ops)


