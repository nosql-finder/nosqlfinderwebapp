from django import template

register = template.Library()

@register.filter
def change_tag(data):
    data = [d['queries'] for d in data]
    try:
        actual = data[-1]
        last = data[-2]
        if actual < last:
            change = 'less'
        elif actual > last:
           change = 'more'
        else:
            change = 'same'
    except IndexError:
        change = "there's no sufficient data"
    return change