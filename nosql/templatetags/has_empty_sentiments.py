from django import template

register = template.Library()

@register.filter
def has_empty_sentiments(sentiments):
    empty = sentiments['positive'] == 0 and sentiments['negative'] == 0
    empty = empty and sentiments['neutral'] == 0
    return empty
