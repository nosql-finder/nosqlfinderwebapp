from django import template

register = template.Library()

@register.filter
def percentage_difference(data):
    data = [d['queries'] for d in data]
    try:
        v1 = data[-1]
        v2 = data[-2]
        result = (abs(v1 - v2)) / ((v1 + v2) / 2)
        result *= 100
    except:
        result = 0
    return round(result, 2)