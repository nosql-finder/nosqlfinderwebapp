from django import template

register = template.Library()

@register.simple_tag
def get_percentage(actual, max_value):
	max_value = int(max_value)
	actual = int(actual)
	if max_value > 0:
		return int((actual * 100) / max_value)
	return 0