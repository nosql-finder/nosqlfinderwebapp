$(document).ready(function() {

	// Data Model Main Chart
	var chart = AmCharts.makeChart("datamodel-main-chart", {
		  "type": "serial",
		  "theme": "light",
		  "hideCredits": true,
		  "marginTop": 30,
		  "paddingTop": 70,
		  "marginBottom": 0,
		  "marginLeft": 30,
		  "marginRight": 30,
		  "autoMargins": false,
		  "color": "#6ab7f3",
		  "dataProvider": dataModelsMainChart,
		  "graphs": [{
		    "lineColor": "#28d8b2",
		    "title": "Positive Comments",
		    "fillAlphas": 1,
		    "lineAlpha": 1,
		    "type": "column",
		    "valueField": "value1",
		    "balloonText": "[[title]]<br /><span style='font-size: 16px;'>[[value]]</span>"
		  },
		  {

		    "lineColor": "#367BF2",
		    "title": "Negative Comments",
		    "fillAlphas": 1,
		    "lineAlpha": 1,
		    "type": "column",
		    "valueField": "value2",
		    "balloonText": "[[title]]<br /><span style='font-size: 16px;'>[[value]]</span>"
		  }
		  ],
		  "categoryField": "category",
		  "categoryAxis": {
		    "position": "top",
		    "gridAlpha": 0,
		    "axisAlpha": 0,
		    "color": "#aaa",
		    "tickLength": 0,
		    "guides": guidesDataModelsMainChart
		  },
		  "valueAxes": [{
		    "axisAlpha": 0,
		    "gridAlpha": 0,
		    "labelsEnabled": false
		  }],
		  "chartCursor": {
		    "fullWidth": true,
		    "cursorAlpha": 0.1,
		    "lineAlpha": 0.1,
		    "cursorColor": "#7B9CF9",
		    "categoryBalloonColor": "#7B9CF9"
		  },
		  "balloon": {
		    "adjustBorderColor": false,
		    "borderAlpha": 1,
		    "shadowAlpha": 0,
		    "borderThickness": 1,
		    "fillAlpha": 1,
		    "color": "#fff"
		  }
	});


	function addHtml(uri, container, data) {
      $(container).html(data);
    };

    var callWhenReady = function (selector, callback, scope) {
        var self = this;
        if ($(selector).closest('body').length) {
            callback.call(scope);
        } else {
            setTimeout(function () {
                callWhenReady(selector, callback, scope);
            }, 1);
        }
    };

    function callApi(url, uri, handler, container) {
		$.ajax({
		    url: url,
		    data: {
		      'uri': uri
		    },
		    beforeSend: function(){
		        $(container).html('<div class="preloader3 loader-block">' +
                '<div class="circ1 loader-cyan"></div>' +
                '<div class="circ2 loader-cyan"></div>' +
                '<div class="circ3 loader-cyan"></div>' +
                '<div class="circ4 loader-cyan"></div>');
		    },
		    success: function (data) {
		        handler(uri, container, data);
		    }
		});
	};

	function handleTabClick(event) {
		if(! $(this).hasClass("done")) {
			var uri = $(this).attr("data-uri");
			if($(this).hasClass("nosql-drivers")) {
				url = '/nosql/api/repositories-by-language';
				container = $('#' + 'drivers-' + uri);
				callApi(url, uri, addHtml, container);
				$(this).addClass("done");
			}
			if($(this).hasClass("nosql-performance")) {
				url = '/nosql/api/sentiments';
				container = $("#" + "performance-" + uri);
				callApi(url, uri, addHtml, container);
				id = uri + "-performance-chart";
				callWhenReady('#' + id,
                    function(){
                        positive = $("#" + id).data("positive");
                        negative = $("#" + id).data("negative");
                        neutral = $("#" + id).data("neutral");
                        total = $("#" + id).data("total");
                        $('#' + uri + '-performance-total').text('from ' + total + ' comments');

                        Chart.defaults.global.legend.labels.usePointStyle = true;
						var ctx = document.getElementById(id);
						var data = {
							labels:["Positive","Negative","Neutral"],
							datasets:[
								{
									data:[positive, negative, neutral],
									backgroundColor: [
										"#28d8b2",
										"#FC6180",
										"#1880C9"
									],
									borderWidth:["2px","2px","2px","2px"],
									borderColor:["#FFf","#fff","#fff"]
								}
							],
						};
						var options = {
					        legend: {
					            display: true,
					            position: 'left',
					            labels: {
					                fontColor: '#333'
					            }
					        },
					        cutoutPercentage: 70
					    };
						new Chart(ctx,{type:'doughnut', data:data, options: options});
                	}
                );
                $(this).addClass('done');
			}
			if($(this).hasClass("nosql-popularity")) {
				url = '/nosql/api/popularity';
				container = $("#popularity-" + uri);
				callApi(url, uri, addHtml, container);
				callWhenReady('#popularity-' + uri + ' canvas',
					function() {
						canvas = $(container).find('canvas');
						years = $(canvas).data('years').split(',');
						comments = String($(canvas).data('sentiment')).split(',');
						commentData = [];
						for(i=0; i < years.length; i++){
							commentData.push({'year': years[i], 'comment': comments[i]});
						}
						var groupedByYear = _.groupBy(commentData, function(item) {
    						return item.year.substring(0,4);
						});
						values = _.map(groupedByYear, function(items){ return items.length });
						labels = Object.keys(groupedByYear)

						var filledLineChart = $(container).find('canvas').get(0).getContext("2d");

            			primaryGradient = filledLineChart.createLinearGradient(0, 0, 0, 100);
            			primaryGradient.addColorStop(0, 'rgba(24,128,201,0.5)');
            			primaryGradient.addColorStop(1, 'rgba(24,128,201,0)');

			            // line chart data
			            var filledLineData = {
			                labels: labels,
			                datasets: [{
			                    label: "Comments",
			                    fill: true,
			                    backgroundColor: primaryGradient,
			                    borderColor: 'rgba(24,128,201,0.5)',
			                    borderCapStyle: 'butt',
			                    borderWidth: 2,
			                    borderDash: [],
			                    borderDashOffset: 0.0,
			                    borderJoinStyle: 'miter',
			                    pointBorderColor: "transparent",
			                    pointBackgroundColor: 'rgba(24,128,201,0.5)',
			                    pointBorderWidth: 0,
			                    pointHoverRadius: 4,
			                    pointHoverBackgroundColor: 'rgba(24,128,201,0.5)',
			                    pointHoverBorderColor: 'rgba(24,128,201,0.5)',
			                    pointHoverBorderWidth: 0,
			                    pointRadius: 3,
			                    pointHitRadius: 10,
			                    data: values
			                }]
			            };

			            if(values.length == 1) {
			            	type = 'bar';
			            } else {
			            	type = 'line';
			            }
			            // line chart init
			            var filledLineChart = new Chart(filledLineChart, {
			                type: type,
			                data: filledLineData,
			                options: {
			                    legend: {
			                        display: false
			                    },
			                    scales: {
			                        xAxes: [{
			                            ticks: {
			                                fontSize: '11',
			                                fontColor: '#a5b5c5'
			                            },
			                            gridLines: {
			                                color: '#f1f1f1',
			                                zeroLineColor: '#f1f1f1'
			                            }
			                        }],
			                        yAxes: [{
			                            ticks: {
			                                beginAtZero: true,
			                                fontSize: '11',
			                                fontColor: '#a5b5c5'
			                            },
			                            gridLines: {
			                                color: 'transparent',
			                                zeroLineColor: 'transparent'
			                            }
			                        }]
			                    }
			                }
			            });
			            $('[data-toggle="tooltip"]').tooltip();
					}
				);
				
				$(this).addClass("done");

			}
		}
	};

	// Get the data when clicked every tab
	$("li.nav-item a").bind("click", handleTabClick);

	// To Draw the interest over time Chart we use a different call to wait
	// that the element gets drawn
	function drawInterestChart(url, uri, handler, container) {
		$.when($.ajax({
		    url: url,
		    data: {
		      'uri': uri
		    },
		    beforeSend: function(){
		        $(container).html('<div class="preloader3 loader-block">' +
	            '<div class="circ1 loader-cyan"></div>' +
	            '<div class="circ2 loader-cyan"></div>' +
	            '<div class="circ3 loader-cyan"></div>' +
	            '<div class="circ4 loader-cyan"></div>');
		    },
		    success: function (data) {
		        handler(uri, container, data);
		    }
		})).done(function(){ 
				uri = $(container).data('uri');
				canvasId = 'interest-over-time-chart-canvas-' + uri; 

				yearStr = $('#' + canvasId).data('years');
				interestStr = $('#' + canvasId).data('interest');

				years = yearStr.split(",");
				years = _.map(years, Number);

				interest = interestStr.split(",");
				interest = _.map(interest, Number);
			
				interestDifference = $('#' + canvasId).data('difference');
				interestSpan = $('#interest-span-' + uri);
				if(interest.length > 0) {
					if (interest[interest.length - 1] > interest[interest.length - 2]){
						icon = '<i class="fa fa-arrow-up p-r-5 text-success"></i>'
					} else if (interest[interest.length - 1] < interest[interest.length - 2]) {
						icon = '<i class="fa fa-arrow-down p-r-5 text-danger"></i>'
					} else {
						icon = '<i class="fa fa-arrow-right p-r-5 text-primary"></i>'
					}
				}
				interestSpan.html(icon + interestDifference + '%');

				var filledLineChart = document.getElementById(canvasId).getContext('2d');

		        primaryGradient = filledLineChart.createLinearGradient(0, 0, 0, 70);
		        primaryGradient.addColorStop(0, 'rgba(40,216,178,0.5)');
  				primaryGradient.addColorStop(1, 'rgba(40,216,178,0)');

		        // line chart data
		        var filledLineData = {
		            labels: years,
		            datasets: [{
		                label: "Google Queries",
		                fill: true,
		                backgroundColor: primaryGradient,
		                borderColor: 'rgba(40,216,178,0.5)',
		                borderCapStyle: 'butt',
		                borderWidth: 2,
		                borderDash: [],
		                borderDashOffset: 0.0,
		                borderJoinStyle: 'miter',
		                pointBorderColor: "transparent",
		                pointBackgroundColor: 'rgba(40,216,178,0.5)',
		                pointBorderWidth: 0,
		                pointHoverRadius: 4,
		                pointHoverBackgroundColor: 'rgba(40,216,178,0.5)',
		                pointHoverBorderColor: 'rgba(40,216,178,0.5)',
		                pointHoverBorderWidth: 0,
		                pointRadius: 3,
		                pointHitRadius: 10,
		                data: interest
		            }]
		        };

		        // line chart init
		        var filledLineChart = new Chart(filledLineChart, {
		            type: 'line',
		            data: filledLineData,
		            options: {
		                legend: {
		                    display: false
		                },
		                scales: {
		                    xAxes: [{
		                        ticks: {
		                            fontSize: '11',
		                            fontColor: '#a5b5c5'
		                        },
		                        gridLines: {
		                            color: '#f1f1f1',
		                            zeroLineColor: '#f1f1f1'
		                        }
		                    }],
		                    yAxes: [{
		                        ticks: {
		                            fontSize: '11',
		                            fontColor: '#a5b5c5'
		                        },
		                        gridLines: {
		                            color: 'transparent',
		                            zeroLineColor: 'transparent'
		                        }
		                    }]
		                }
		            }
		        });
			});
	}		

	var charts = $('[data-toggle="interest-over-time-chart"]');
	for (i=0; i < charts.length; i++){
		url = '/nosql/api/interest-over-time';
		var uri = $(charts[i]).attr("data-uri");
		drawInterestChart(url, uri, addHtml, $(charts[i]));
		
	}

	// Recovered Data Gauges
	var gaugeOptions = {
	    lines: 12, // The number of lines to draw
	    angle: 0.5, // The length of each line
	    lineWidth: 0.08, // The line thickness
	    pointer: {
	      length: 0.9, // The radius of the inner circle
	      strokeWidth: 0.035, // The rotation offset
	      color: '#000000' // Fill color
	    },
	    limitMax: 'false',   // If true, the pointer will not go past the end of the gauge
	    colorStart: '#4981FD',
	    strokeColor: '#d1d4d7',   // to see which ones work best for you
	    generateGradient: true
  	};

  	function getGaugeColor(value) {
  		if(value === 100){
  			color = '#19b3a4'
  		} else if(value < 100 && value >= 70) {
  			color = '#4981fd'
  		} else if(value < 70 && value > 60) {
  			color = '#ff9800'
  		} else {
  			color = '#f75089'
  		}
  		return color;
  	}

	gauges = $('[data-toggle="mini-gauge"]')
	for(i=0; i < gauges.length; i++){
		value = $(gauges[i]).data("value");
	  	value = parseInt(value);
	  	color = getGaugeColor(value);
	  	gaugeOptions['colorStart'] = color;
	  	gauge = new Donut(gauges[i]).setOptions(gaugeOptions);
		gauge.maxValue = 100;
	  	gauge.animationSpeed = 32;
	  	gauge.set(value);
	}

	// Initialice Carousels
	$('.carousel-nav').owlCarousel({
		items: 3, 
		loop: false, 
		autoplay: false, 
		nav: false, 
		margin:30
	});

	$('.carousel-dot').owlCarousel({
		items: 3,
		loop: false,
		autoplay: false,
		nav: false, 
		margin:30
	});

	$('.multiple-items').slick({
		dots: true,
		adaptiveHeight: true,
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 3,
		responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 3,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		]
});

	// Quick Tour Option
	$('#quick-tour').bind('click', function(){
		introJs().start();
	});

});