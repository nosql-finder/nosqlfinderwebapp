# -*- coding: utf-8 -*-
##############################################################################
# Class to get ranking for popularity in different websites
##############################################################################
from neomodel import db
from .utils import get_first_or_none

class SiteStats:

    def __init__(self):

        self.stackoverflow_views_query = """
        MATCH (n:NoSQL)-[:SAME_AS]-(st:StackoverflowTag)
        MATCH (n)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
        OPTIONAL MATCH (n)-[:HAS]-(rev:Review)
        WITH n, st, rev, y
        ORDER BY y.year
        RETURN n.name, st.viewed, rev.total_rating, COLLECT(y) ORDER BY st.viewed DESC
        {optional_limit}"""

        self.repository_stars_query = """
        MATCH (n:NoSQL)-[:HAS_A]-(re:Repository)
        MATCH (n)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
        OPTIONAL MATCH (n)-[:HAS]-(rev:Review)
        WITH n, re, rev, y
        ORDER BY y.year
        RETURN n.name, re.stars, rev.total_rating, COLLECT(y) ORDER BY re.stars DESC
        {optional_limit}"""

        self.stackshare_votes_query = """
        MATCH (n:NoSQL)-[:SAME_AS]-(st:StacksharePost)
        MATCH (n)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
        OPTIONAL MATCH (n)-[:HAS]-(rev:Review)
        WITH n, st, rev, y
        ORDER BY y.year
        RETURN n.name, st.votes, rev.total_rating, COLLECT(y) ORDER BY st.votes DESC
        {optional_limit}"""

        self.alternativeto_likes_query = """
        MATCH (n:NoSQL)-[:SAME_AS]-(al:AlternativetoPost)
        MATCH (n)-[:HAS]-(:InterestOverTime)-[:HAS]-(y:Year)
        OPTIONAL MATCH (n)-[:HAS]-(rev:Review)
        WITH n, al, rev, y
        ORDER BY y.year
        RETURN n.name, al.likes, rev.total_rating, COLLECT(y) ORDER BY al.likes DESC
        {optional_limit}"""

    ######################################################################
    # The 'stackoverflow_ranking' is based on the products with the most
    # views in their stackoverflow tag
    #####################################################################

    def get_position_in_stackoverflow_ranking(self, product_name):
        ranking = self._get_ranking(self.stackoverflow_views_query, 0)
        position = self._get_position_in_ranking(ranking, product_name)
        return position

    def get_stackoverflow_ranking(self, number_positions=3):
        ranking = self._get_ranking(self.stackoverflow_views_query,
                                    number_positions)
        ranking = self._add_percentage_field_based_on_max(ranking)
        return ranking


    ######################################################################
    # The 'repositories_ranking' is based on the products with the most
    # number of stars in their github repository
    #####################################################################

    def get_position_in_repositories_ranking(self, product_name):
        ranking = self._get_ranking(self.repository_stars_query, 0)
        position = self._get_position_in_ranking(ranking, product_name)
        return position

    def get_repositories_ranking(self, product_name, number_positions=3):
        ranking = self._get_ranking(self.repository_stars_query,
                                    number_positions)
        ranking = self._add_percentage_field_based_on_max(ranking)
        return ranking

    ######################################################################
    # The 'stackshare_ranking' is based on the products with the most
    # number of votes in the stackshare website
    #####################################################################

    def get_position_in_stackshare_ranking(self, product_name):
        ranking = self._get_ranking(self.stackshare_votes_query, 0)
        position = self._get_position_in_ranking(ranking, product_name)
        return position

    def get_stackshare_ranking(self, product_name, number_positions=3):
        ranking = self._get_ranking(self.stackshare_votes_query,
                                    number_positions)
        ranking = self._add_percentage_field_based_on_max(ranking)
        return ranking

    ######################################################################
    # The 'alternativeto_ranking' is based on the products with the most
    # number of likes in the alternativeto website
    #####################################################################

    def get_position_in_alternativeto_ranking(self, product_name):
        ranking = self._get_ranking(self.alternativeto_likes_query, 0)
        position = self._get_position_in_ranking(ranking, product_name)
        return position

    def get_alternativeto_ranking(self, product_name, number_positions=3):
        ranking = self._get_ranking(self.alternativeto_likes_query,
                                    number_positions)
        ranking = self._add_percentage_field_based_on_max(ranking)
        return ranking

    def _get_ranking(self, query, number_positions):
        limit = self._get_query_limit(number_positions)
        query = query.format(optional_limit=limit)
        results, meta = db.cypher_query(query)
        nosqls = []
        for i, r in enumerate(results):
            data = {}
            data['position'] = i + 1
            data['name'] = r[0]
            data['value'] = r[1]
            data['rating'] = r[2]
            data['interest_over_time'] = [i.properties for i in r[3]]
            nosqls.append(data)
        return nosqls

    def _get_query_limit(self, number_positions):
        if number_positions > 0:
            limit = "LIMIT {}".format(number_positions)
        else:
            limit = ""
        return limit

    def _get_position_in_ranking(self, ranking, product_name):
        for i in range(len(ranking)):
            if ranking[i]['name'] == product_name:
                return i + 1
        return -1

    def _add_percentage_field_based_on_max(self, ranking):
        result = []
        if ranking:
            values = [r['value'] for r in ranking]
            if not self._is_none_in_list(values):
                max_value = max(values)
                result = self._add_percent_to_field(ranking, max_value)
        return result

    def _add_percent_to_field(self, items, max_value):
        fields = []
        for item in items:
            if max_value > 0:
                item['percent'] = int((item['value'] * 100) / max_value)
            else:
                item['percent'] = 0
            fields.append(item)
        return fields

    def _is_none_in_list(self, values):
        return None in values
