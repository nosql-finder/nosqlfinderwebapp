# -*- coding: utf-8 -*-
##############################################################################
# Class to get the data for the bullet charts in the technical details tab
##############################################################################
from neomodel import db
from .utils import get_first_or_none


class BulletStats:

    def __init__(self):
        self.stackoverflow_views_query = """
        OPTIONAL MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(st:StackoverflowTag)
        WITH st
        MATCH (:NoSQL)-[:SAME_AS]-(st2:StackoverflowTag)
        RETURN st.views, MIN(st2.views), AVG(st2.views), MAX(st2.views)
        """

        self.repository_stars_query = """
        OPTIONAL MATCH (n:NoSQL {{uri: '{}'}})-[:HAS]-(re:Repository)
        WITH re
        MATCH (n:NoSQL)-[:HAS]-(re2:Repository)
        RETURN re.stars, MIN(re2.stars), AVG(re2.stars), MAX(re2.stars)
        """

        self.stackshare_votes_query = """
        OPTIONAL MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(st:StacksharePost)
        WITH st
        MATCH (n:NoSQL)-[:SAME_AS]-(st2:StacksharePost)
        RETURN st.votes, MIN(st2.votes), AVG(st2.votes), MAX(st2.votes)
        """

        self.alternativeto_likes_query = """
        OPTIONAL MATCH (n:NoSQL {{uri:'{}'}})-[:SAME_AS]-(al:AlternativetoPost) 
        WITH al 
        MATCH (n:NoSQL)-[:SAME_AS]-(al2:AlternativetoPost)
        RETURN al.likes, MIN(al2.likes), AVG(al2.likes), MAX(al2.likes)
        """

        self.dbengine_score_query = """
        OPTIONAL MATCH (n:NoSQL {{uri: '{}'}})-[:SAME_AS]-(db:DBEnginePost)
        WITH db
        MATCH (:NoSQL)-[:SAME_AS]-(db2:DBEnginePost)
        RETURN db.score, MIN(db2.score), AVG(db2.score), MAX(db2.score)
        """

    ######################################################################
    # The 'stackoverflow_views' finds the views for the product and the
    # minimum views and maximum views for a clear context of the data
    #####################################################################

    def get_stackoverflow_views(self, uri):
        query = self.stackoverflow_views_query.format(uri)
        stats = self._get_result(query)
        return stats

    def get_repository_stars(self, uri):
        query = self.repository_stars_query.format(uri)
        stats = self._get_result(query)
        return stats

    def get_stackshare_votes(self, uri):
        query = self.stackshare_votes_query.format(uri)
        stats = self._get_result(query)
        return stats

    def get_alternativeto_likes(self, uri):
        query = self.alternativeto_likes_query.format(uri)
        stats = self._get_result(query)
        return stats

    def get_dbengine_score(self, uri):
        query = self.dbengine_score_query.format(uri)
        stats = self._get_result(query)
        return stats

    def _get_result(self, query):
        results, meta = db.cypher_query(query)
        result = get_first_or_none(results)
        if result:
            stats = {'min': result[1], 'avg': result[2], 'max': result[3]}
            actual = result[0]
            if not actual:
                actual = 0
            stats['actual'] = actual
        else:
            stats = {}
        return stats