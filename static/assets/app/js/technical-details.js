$(document).ready(function() {

    // Slim Scrolls
    $('#supported-apis').slimScroll({
        height: '180px'
    });

    $('#operating-systems').slimScroll({
        height: '240px'
    });

    // Stackshare Comments Chart
    if($("#stackshare-comments").length){
        commentChart = AmCharts.makeChart("stackshare-comments", {
            "type": "serial",
            "theme": "light",
            "hideCredits": true,
            "dataProvider": stackshareComments,
            "startDuration": 1,
            "categoryField": "text",
            "categoryAxis": {
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF",
                "axisColor": "#555555",
                "labelsEnabled": false,
                "color": "#fff",
            },
            "valueAxes": [{
                "id": "a1",
                "gridAlpha": 0,
                "axisAlpha": 0,
                "labelsEnabled": false
            }],
            "graphs": [{
                "id": "g1",
                "valueField": "likes",
                "type": "column",
                "fillAlphas": 0.9,
                "valueAxis": "a1",
                "balloonText": "[[value]] likes",
                "legendValueText": "[[value]] mi",
                "legendPeriodValueText": "total: [[value.sum]] mi",
                "lineColor": "#2cc4cb",
                "alphaField": "alpha",
                "color": "#fff"
            }],
            "chartScrollbar": {
              "oppositeAxis": false,
              "graph": "g1",
              "scrollbarHeight": 20,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount":false,
                "color":"#AAAAAA"
                    },
             "chartCursor": {
                "cursorAlpha": 0.1,
                "cursorColor":"#2cc4cb",
                 "fullWidth":true,
                "valueBalloonsEnabled": true,
                "zoomable": true
            }  
        });
        commentChart.addListener( "rendered", zoomChart );
        zoomChart();

        function zoomChart() {
          commentChart.zoomToIndexes( 0, 5 );
        } 
    }

    // Comparison Charts
    function drawComparisonChart(elementId, data, guideIndex) {
        comparisonChartOptions = {
            "type": "serial",
            "hideCredits":true,
            "theme": "light",
            "marginRight": 40,
            "marginLeft": 40,
            "autoMarginOffset": 20,
            "mouseWheelZoomEnabled":true,
            "valueAxes": [{
                "id": "v1",
                "axisAlpha": 0,
                "dashLength":6,
                "position": "left",
                "gridAlpha":0.1,
                "ignoreAxisWidth":true
            }],
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "graphs": [{
                "id": "g1",
                "balloon":{
                  "drop":false,
                  "adjustBorderColor":false,
                  "color":"#ffffff"
                },
                "fillAlphas": 0.5,
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 7,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "lineColor":"#3ACCB5",
                "lineThickness":2,
                "negativeLineColor":"#3ACCB5",
                "type":"smoothedLine",
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis":false,
                "offset":30,
                "scrollbarHeight": 80,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount":false,
                "color":"#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha":1,
                "cursorColor":"#3ACCB5",
                "limitToGraph":"g1",
                "valueLineAlpha":0.2,
                "valueZoomable":true
            },
            "valueScrollbar":{
              "oppositeAxis":false,
              "offset":50,
              "scrollbarHeight":10
            },
            "categoryField": "name",
            "categoryAxis": {
                "gridAlpha":0,
                "axisAlpha":0,
                "labelsEnabled": false,
                "guides": [{
                  "category": productName,
                  "above": true,
                  "lineColor": "#3ACCB5",
                  "lineAlpha": 5,
                  "fillColor": "#3ACCB5",
                  "dashLength": 2,
                  "inside": false,
                  "labelRotation": 90,
                  "label": productName,
                  "balloonText": "Additional information",
                  "tickLength": 10

                }]
            },
            "export": {
                "enabled": false
            },
            "dataProvider": data
        };

        var comparisonChart = AmCharts.makeChart(elementId, 
        comparisonChartOptions);
        comparisonChart.addListener("rendered", zoomChart);
        zoomChart();

        function zoomChart() {
            comparisonChart.zoomToIndexes(guideIndex, 
            comparisonChart.dataProvider.length - 1);
        }
    }
    drawComparisonChart("stackoverflow-comparison-chart", comparisonStackoverflow, nextStackoverflowIndex);
    drawComparisonChart("github-comparison-chart", comparisonGithub, nextGithubIndex);
    drawComparisonChart("dbengine-comparison-chart", comparisonDBEngine , nextDBEngineIndex);
    drawComparisonChart("stackshare-comparison-chart", comparisonStackshare , nextStackshareIndex);
    drawComparisonChart("alternativeto-comparison-chart", comparisonAlternativeto, nextAlternativetoIndex)
    


    // // Interest Over Time Chart
    // interestOverTimeChartOptions = {
    //     "type": "serial",
    //     "hideCredits":true,
    //     "theme": "light",
    //     "marginRight": 40,
    //     "marginLeft": 60,
    //     "autoMarginOffset": 20,
    //     "mouseWheelZoomEnabled":false,
    //     "valueAxes": [{
    //         "id": "v1",
    //         "axisAlpha": 0,
    //         "dashLength":6,
    //         "position": "left",
    //         "gridAlpha":0.1,
    //         "ignoreAxisWidth":true
    //     }],
    //     "balloon": {
    //         "borderThickness": 1,
    //         "shadowAlpha": 0
    //     },
    //     "graphs": [{
    //         "id": "g1",
    //         "balloon":{
    //           "drop":true,
    //           "adjustBorderColor":false,
    //           "color":"#ffffff"
    //         },
    //         "fillAlphas": 0.2,
    //         "bullet": "round",
    //         "bulletBorderAlpha": 1,
    //         "bulletColor": "#FFFFFF",
    //         "bulletSize": 7,
    //         "hideBulletsCount": 50,
    //         "lineThickness": 2,
    //         "lineColor":"#2cc4cb",
    //         "negativeLineColor":"#4680ff",
    //         "title": "red line",
    //         "useLineColorForBulletBorder": true,
    //         "valueField": "value",
    //         "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
    //     }],
        
    //     "chartCursor": {
    //         "pan": true,
    //         "valueLineEnabled": true,
    //         "valueLineBalloonEnabled": true,
    //         "cursorAlpha":1,
    //         "cursorColor":"#258cbb",
    //         "limitToGraph":"g1",
    //         "valueLineAlpha":0.2,
    //         "valueZoomable":true
    //     },
        
    //     "categoryField": "year",
    //     "categoryAxis": {
    //         "gridAlpha":0,
    //         "axisAlpha":0,
    //         "labelsEnabled": false
    //     },
    //     "export": {
    //         "enabled": false
    //     },
    //     "dataProvider": interestOverTimeData
    // };

    // var interestOverTimeChart = AmCharts.makeChart("interest-over-time-chart", interestOverTimeChartOptions);
    var interestOverTimeContainer = document.getElementById("interest-over-time-chart").getContext('2d');

    primaryGradient = interestOverTimeContainer.createLinearGradient(0, 0, 0, 200);
    primaryGradient.addColorStop(0, 'rgba(40,216,178,0.5)');
    primaryGradient.addColorStop(1, 'rgba(40,216,178,0)');

    var interestOverTimeData = {
        labels: interestOverTimeLabels,
        datasets: [{
            label: "Google Queries",
            fill: true,
            backgroundColor: primaryGradient,
            borderColor: 'rgba(40,216,178,0.5)',
            borderCapStyle: 'butt',
            borderWidth: 2,
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: "transparent",
            pointBackgroundColor: 'rgba(40,216,178,0.5)',
            pointBorderWidth: 0,
            pointHoverRadius: 6,
            pointHoverBackgroundColor: 'rgba(40,216,178,0.5)',
            pointHoverBorderColor: 'rgba(40,216,178,0.5)',
            pointHoverBorderWidth: 0,
            pointRadius: 4,
            pointHitRadius: 10,
            data: interestOverTimeQueries
        }]
    };

    var interestOverTimeChart = new Chart(interestOverTimeContainer, {
        type: 'line',
        data: interestOverTimeData,
        options: {
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        fontSize: '11',
                        fontColor: '#a5b5c5'
                    },
                    gridLines: {
                        color: '#f1f1f1',
                        zeroLineColor: '#f1f1f1'
                    }
                }],
                yAxes: [{
                    ticks: {
                        fontSize: '11',
                        fontColor: '#a5b5c5'
                    },
                    gridLines: {
                        color: 'transparent',
                        zeroLineColor: 'transparent'
                    }
                }]
            }
        }
    });


    function handleTabClick(event){
        if(! $(this).hasClass("done")) {
            var link = $(this).find('a.nav-link');
            href = $(link).attr('href');
            href = href.replace('#', '');
            href = $.trim(href);
        }
             
     };

     $(".comparision-chart-link li").bind("click", handleTabClick);

     

    
});
