$(document).ready(function() {

	// Github Drivers Chart
    if($("#github-drivers-chart").length){
    	AmCharts.makeChart("github-drivers-chart", {
            "type": "serial",
            "theme": "light",
            "hideCredits": true,
            "dataProvider": driverReposData,
            "startDuration": 1,
            "categoryField": "name",
            "categoryAxis": {
                "autoGridCount": true,
                "gridAlpha": 0,
                "gridColor": "#FFFFFF",
                "axisColor": "#555555",
                "labelsEnabled": true,
                "color": "#919aa3",
            },
            "sequencedAnimation": "easeInSine",
            "valueAxes": [{
                "id": "a1",
                "title": "Repositories",
                "gridAlpha": 0.1,
                "axisAlpha": 0,
                "gridColor": "#919aa3"
            }],
            "graphs": [{
                "id": "g1",
                "valueField": "value",
                "title": "distance",
                "type": "column",
                "fillAlphas": 0.9,
                "valueAxis": "a1",
                "balloonText": "[[value]] ratio",
                "legendValueText": "[[value]] mi",
                "legendPeriodValueText": "total: [[value.sum]] mi",
                "lineColor": "#2cc4cb",
                "alphaField": "alpha",
                "color": "#fff"
            }],
             "chartCursor": {
                "cursorAlpha": 0.1,
                "cursorColor":"#2cc4cb",
                 "fullWidth":true,
                "valueBalloonsEnabled": true,
                "zoomable": true
            }
        });
    }

    if($("#stackoverflow-related-tech-charts").length){
        AmCharts.makeChart("stackoverflow-related-tech-charts", {
            "type": "serial",
            "theme": "light",
            "hideCredits": true,
            "dataProvider": stackoverflowTechnologiesData,
            "startDuration": 1,
            "categoryField": "name",
            "categoryAxis": {
                "autoGridCount": true,
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF",
                "axisColor": "#555555",
                "labelsEnabled": true,
                "color": "#fff",
            },
            "valueAxes": [{
                "id": "a1",
                "title": "Ratio",
                "gridAlpha": 0,
                "axisAlpha": 0
            }],
            "graphs": [{
                "id": "g1",
                "valueField": "value",
                "title": "distance",
                "type": "column",
                "fillAlphas": 0.9,
                "valueAxis": "a1",
                "balloonText": "[[value]] ratio",
                "legendValueText": "[[value]] mi",
                "legendPeriodValueText": "total: [[value.sum]] mi",
                "lineColor": "#2cc4cb",
                "alphaField": "alpha",
                "color": "#fff"
            }],
             "chartCursor": {
                "cursorAlpha": 0.1,
                "cursorColor":"#2cc4cb",
                 "fullWidth":true,
                "valueBalloonsEnabled": true,
                "zoomable": true
            }  
        });
    }

    if($("#github-related-tech-charts").length){
        AmCharts.makeChart("github-related-tech-charts", {
        "type": "serial",
        "theme": "light",
            "hideCredits": true,
            "dataProvider": githubTechnologiesData,
            "startDuration": 1,
            "categoryField": "name",
            "categoryAxis": {
                "autoGridCount": true,
                "gridAlpha": 0.1,
                "gridColor": "#FFFFFF",
                "axisColor": "#555555",
                "labelsEnabled": true,
                "color": "#fff",
            },
            "valueAxes": [{
                "id": "a1",
                "title": "Repositories",
                "gridAlpha": 0,
                "axisAlpha": 0
            }],
            "graphs": [{
                "id": "g1",
                "valueField": "value",
                "title": "distance",
                "type": "column",
                "fillAlphas": 0.9,
                "valueAxis": "a1",
                "balloonText": "[[value]] repositories",
                "legendValueText": "[[value]] mi",
                "legendPeriodValueText": "total: [[value.sum]] mi",
                "lineColor": "#2cc4cb",
                "alphaField": "alpha",
                "color": "#fff"
            }],
             "chartCursor": {
                "cursorAlpha": 0.1,
                "cursorColor":"#2cc4cb",
                 "fullWidth":true,
                "valueBalloonsEnabled": true,
                "zoomable": true
        }
        });
    }
});